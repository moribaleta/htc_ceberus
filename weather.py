#!/usr/bin/python

import cgi, cgitb
import json
import urllib2

form = cgi.FieldStorage()
lat = form.getvalue('lat')
lon = form.getvalue('lon')

#url = 'http://api.openweather.org/data/2.5/weather?lat=' + str(lat) + '&lon=' + str(lon) + '&appid=f7d05e671dc29ce9227fdae70f7376b1'

## SAMPLE.JSON FILE FOR FUCKING
f = open('/usr/lib/cgi-bin/sample.json', 'r')
json_string = f.read()
#w = json.loads(json_string)

## LEGIT URL REQ
#quick and dirty approach
def fetchHTML(url):
    req = urllib2.Request(url)
    response = urllib2.urlopen(req)
    return response.read()

#json_string = fetchHTML(url)
w = json.loads(json_string)

print "Content-type: text/html\n"

def get_name():
    return w['name']

def get_country():
    return w['sys']['country']

def get_temp_c():
    return (float(w['main']['temp']) - 273.15)

def get_rain():
    for s in w:
        if s == 'rain':
            return w['rain']
        else: return 'none'

def get_clouds():
    return w['weather'][0]['description']

def get_wind():
    speed = 0
    deg = 0
    
    for entry in w:
        if entry == 'wind':
            for attrib in entry:
                if attrib == 'speed':
                    speed = float(w['wind']['speed']) * 1.60934
    return speed

def get_visibility():
    v = w['visibility']
    degree = ''#very low, low, med, high
    if v >= 100000:
        degree = 'High'
    elif v <= 100000 and v >= 50000:
        degree = 'Medium'
    elif v <= 50000 and v >= 10000:
        degree = 'Low'
    elif v <= 10000:
        degree = 'Very Low'
    return str(v) + ' meters, ' + degree

wind = get_wind()
print get_name()
print get_country()
print str(get_temp_c()) + ' C'
print get_rain()
print get_clouds()
print str(get_wind())
print get_visibility()
print (lat, lon)
